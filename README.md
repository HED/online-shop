# Онлайн магазин канцтоваров

Приложение является самой простейшей формой Backend части онлайн магазина канцтоваров.

## Стек

+ Java 11
+ H2
+ Spring Boot 2.6.13
+ Docker (docker-compose) [Kafka, Zookeeper]
+ Swagger 3
+ Maven

## Запуск

Для запуска необходимо освободить порты: 8080 (server), 9092 (kafka), 92092 (kafka), 2181 (zookeeper)

Перед стартом приложения необходимо запустить ./docker/docker-compose.yml командой docker-compose up -d

База данных хранит данные в файлах ./data/demo.mv.db ./data/demo.trace.db

## Взаимодействие

Все доступные возможности представлены в swagger по ссылке http://localhost:8080/swagger-ui/index.html

Для отправки запросов необходимо авторизоваться нажав на кнопку "Authorize🔓". Авторизоваться можно под двумя учётками:
+ *log:* **Seller** *pass:* **123** *role:* **SELLER**
+ *log:* **Manager** *pass:* **321** *role:* **MANAGER**

Авторизация нужна для всех запросов. Большинство эндпоинтов доступно с ролью **SELLER**. 

Эндпоинты доступные только пользователю с ролью **MANAGER**:
+ **PATH** */orders/{id}*
+ **PATH** */orders/{id}/status?status={status}*