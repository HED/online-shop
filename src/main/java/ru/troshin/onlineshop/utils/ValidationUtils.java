package ru.troshin.onlineshop.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Инструменты для валидации
 */
@Slf4j
public class ValidationUtils {

    /**
     * Валидация идентификатора
     *
     * @param objName название объекта
     * @param id идентификатор объекта
     * @return Если валидация не прошла, то текст ошибки, иначе -- Optional.empty
     */
    public static Optional<String> idValidate(String objName, Integer id) {
        log.debug("ValidationUtils#idValidate objName={} id={}", objName, id);
        if (id == null) {
            return Optional.of(objName + " id mustn't be null");
        } else if (id <= 0) {
            return Optional.of(objName + " id must be greater than zero");
        } else {
            return Optional.empty();
        }
    }

    /**
     * Валидация списка идентификаторов
     *
     * @param objName название объекта
     * @param idList список идентификаторов
     * @return Если валидация не прошла, то список описаний ошибок, иначе -- пустой список.
     */
    public static List<String> idListValidate(String objName, List<Integer> idList) {
        log.debug("ValidationUtils#idListValidate objName={} idList={}", objName, idList);
        final var validationErrorMessages = new ArrayList<String>();
        if (idList == null || idList.isEmpty()) {
            validationErrorMessages.add(objName + " must be specified in the order");
        } else {
            idList.forEach(id -> idValidate(objName, id).ifPresent(validationErrorMessages::add));
        }
        return validationErrorMessages;
    }

}
