package ru.troshin.onlineshop.services.api;

import ru.troshin.onlineshop.domain.entity.Product;
import ru.troshin.onlineshop.rest.dto.IdNameDto;

import java.util.List;

/**
 * Сервис получения товаров
 */
public interface ProductService {
    /**
     * Получить список сущностей товаров по идентификаторам
     *
     * @param productsIdList список идентификаторов
     * @return список товаров
     */
    List<Product> findAllByIdList(List<Integer> productsIdList);

    /**
     * Получение списка DTO товаров
     *
     * @return список товаров
     */
    List<IdNameDto> getAll();

    /**
     * Получение DTO товара по идентификатору
     *
     * @param productId идентификатор товара
     * @return товар
     */
    IdNameDto getById(Integer productId);
}
