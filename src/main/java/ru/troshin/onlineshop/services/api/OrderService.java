package ru.troshin.onlineshop.services.api;

import ru.troshin.onlineshop.domain.entity.OrderStatus;
import ru.troshin.onlineshop.rest.dto.CreateOrderRequestDto;
import ru.troshin.onlineshop.rest.dto.OrderResponseDto;
import ru.troshin.onlineshop.rest.dto.UpdateOrderRequestDto;

import java.util.List;

/**
 * Сервис работы с заказами
 */
public interface OrderService {
    /**
     * Получить заказ по идентификатору
     *
     * @param orderId идентификатор заказа
     * @return DTO заказа
     */
    OrderResponseDto get(Integer orderId);

    /**
     * Получить заказы по идентификатору клиента
     *
     * @param customerId идентификатор клиента
     * @return список заказов клиента
     */
    List<OrderResponseDto> getByCustomer(Integer customerId);

    /**
     * Создать заказ
     *
     * @param orderDto DTO нового заказа
     * @return DTO созданного заказа
     */
    OrderResponseDto create(CreateOrderRequestDto orderDto);

    /**
     * Редактировать продукты в заказе
     *
     * @param orderId идентификатор заказа
     * @param orderDto DTO заказа для редактирования
     * @return DTO редактированного заказа
     */
    OrderResponseDto updateOrderProducts(Integer orderId, UpdateOrderRequestDto orderDto);

    /**
     * Изменить статус заказа
     *
     * @param orderId идентификатор заказа
     * @param status новый статус заказа
     * @return DTO изменённого
     */
    OrderResponseDto updateStatus(Integer orderId, OrderStatus status);
}
