package ru.troshin.onlineshop.services.api;


import ru.troshin.onlineshop.rest.dto.OrderResponseDto;
import ru.troshin.onlineshop.rest.dto.OrderState;

/**
 * Сервис работы с кафкой
 */
public interface KafkaProducer {
    /**
     * Отправка сообщения о действии с заказом
     *
     * @param orderId идентификатор заказа
     * @param state   состояние заказа
     */
    void sendMessage(Integer orderId, OrderState state, OrderResponseDto orderDto);
}
