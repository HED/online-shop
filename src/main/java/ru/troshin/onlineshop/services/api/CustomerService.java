package ru.troshin.onlineshop.services.api;

import ru.troshin.onlineshop.domain.entity.Customer;
import ru.troshin.onlineshop.rest.dto.CustomerResponseDto;

import java.util.List;

/**
 * Сервис получения клиентов
 */
public interface CustomerService {
    /**
     * Получить сущность клиента по идентификатору
     *
     * @param customerId идентификатор клиента
     * @return сущность клиента
     */
    Customer findById(Integer customerId);

    /**
     * Получить DTO клиента по идентификатору
     *
     * @param customerId идентификатор клиента
     * @return DTO клиента
     */
    CustomerResponseDto getById(Integer customerId);

    /**
     * Получить список клиентов
     *
     * @return список клиентов
     */
    List<CustomerResponseDto> getAll();
}
