package ru.troshin.onlineshop.services.impl;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.troshin.onlineshop.domain.entity.Product;
import ru.troshin.onlineshop.domain.repository.ProductRepository;
import ru.troshin.onlineshop.rest.dto.IdNameDto;
import ru.troshin.onlineshop.rest.exception.EntityNotFoundException;
import ru.troshin.onlineshop.services.api.ProductService;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final LoadingCache<Integer, Product> cache;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
        this.cache = Caffeine.newBuilder()
                .maximumSize(4)
                .expireAfterWrite(Duration.ofMinutes(10))
                .build(this::find);
    }

    @Override
    @Transactional
    public List<Product> findAllByIdList(List<Integer> productsIdList) {
        log.debug("ProductServiceImpl#findAllByIdList productsIdList={}", productsIdList);
        return new ArrayList<>(cache.getAll(productsIdList).values());
    }

    @Override
    public List<IdNameDto> getAll() {
        log.debug("ProductServiceImpl#getAll");
        //По-хорошему нужно вызывать findAll(pageable), но в рамках данной задачи не имеет смысла
        return productRepository.findAll().stream().map(this::toDto).collect(Collectors.toList());
    }

    @Override
    public IdNameDto getById(Integer productId) {
        log.debug("ProductServiceImpl#getById productId={}", productId);
        return toDto(cache.get(productId));
    }

    /**
     * Поиск товара в БД по идентификатору
     *
     * @param productId идентификатор товара
     * @return сущность товара
     */
    private Product find(Integer productId) {
        log.debug("ProductServiceImpl#find productId={}", productId);
        return productRepository.findById(productId)
                .orElseThrow(() -> new EntityNotFoundException("Product not found by id=" + productId));
    }

    /**
     * Маппинг сущности товара в DTO
     *
     * @param product сущность товара
     * @return DTO товара
     */
    private IdNameDto toDto(Product product) {
        log.debug("ProductServiceImpl#toDto product={}", product);
        return IdNameDto.builder().id(product.getId()).name(product.getName()).build();
    }
}
