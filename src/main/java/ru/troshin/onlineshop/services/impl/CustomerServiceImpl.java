package ru.troshin.onlineshop.services.impl;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.troshin.onlineshop.domain.entity.Customer;
import ru.troshin.onlineshop.domain.repository.CustomerRepository;
import ru.troshin.onlineshop.rest.dto.CustomerResponseDto;
import ru.troshin.onlineshop.rest.exception.EntityNotFoundException;
import ru.troshin.onlineshop.services.api.CustomerService;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final LoadingCache<Integer, Customer> cache;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
        this.cache = Caffeine.newBuilder()
                .maximumSize(3)
                .expireAfterWrite(Duration.ofMinutes(10))
                .build(this::find);
    }

    @Override
    @Transactional
    public Customer findById(Integer customerId) {
        log.debug("CustomerServiceImpl#findById customerId={}", customerId);
        return cache.get(customerId);
    }

    @Override
    public CustomerResponseDto getById(Integer customerId) {
        log.debug("CustomerServiceImpl#getById customerId={}", customerId);
        return toDto(cache.get(customerId));
    }

    @Override
    public List<CustomerResponseDto> getAll() {
        log.debug("CustomerServiceImpl#getAll");
        //По-хорошему нужно вызывать findAll(pageable), но в рамках данной задачи не имеет смысла
        return customerRepository.findAll().stream().map(this::toDto).collect(Collectors.toList());
    }

    /**
     * Поиск клиента в БД по идентификатору
     *
     * @param customerId идентификатор клиента
     * @return сущность клиента
     */
    private Customer find(Integer customerId) {
        log.debug("CustomerServiceImpl#find customerId={}", customerId);
        return customerRepository.findById(customerId)
                .orElseThrow(() -> new EntityNotFoundException("Customer not found by id=" + customerId));
    }

    /**
     * Маппинг сущности клиента в DTO
     *
     * @param customer сущность клиента
     * @return DTO клиента
     */
    private CustomerResponseDto toDto(Customer customer) {
        log.debug("CustomerServiceImpl#toDto customer={}", customer);
        return CustomerResponseDto.builder()
                .id(customer.getId())
                .fullName(customer.getFullName())
                .phoneNumber(customer.getPhoneNumber())
                .build();
    }
}
