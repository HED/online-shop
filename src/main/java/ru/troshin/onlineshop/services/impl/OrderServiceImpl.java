package ru.troshin.onlineshop.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.troshin.onlineshop.domain.entity.Order;
import ru.troshin.onlineshop.domain.entity.OrderStatus;
import ru.troshin.onlineshop.domain.repository.OrderRepository;
import ru.troshin.onlineshop.rest.dto.CreateOrderRequestDto;
import ru.troshin.onlineshop.rest.dto.IdNameDto;
import ru.troshin.onlineshop.rest.dto.OrderResponseDto;
import ru.troshin.onlineshop.rest.dto.OrderState;
import ru.troshin.onlineshop.rest.dto.UpdateOrderRequestDto;
import ru.troshin.onlineshop.rest.exception.BadRequestException;
import ru.troshin.onlineshop.rest.exception.EntityNotFoundException;
import ru.troshin.onlineshop.services.api.CustomerService;
import ru.troshin.onlineshop.services.api.KafkaProducer;
import ru.troshin.onlineshop.services.api.OrderService;
import ru.troshin.onlineshop.services.api.ProductService;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final CustomerService customerService;
    private final ProductService productService;
    private final KafkaProducer kafkaProducer;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository,
                            CustomerService customerService,
                            ProductService productService,
                            KafkaProducer kafkaProducer) {
        this.orderRepository = orderRepository;
        this.customerService = customerService;
        this.productService = productService;
        this.kafkaProducer = kafkaProducer;
    }

    @Override
    @Transactional(readOnly = true)
    public OrderResponseDto get(Integer orderId) {
        log.debug("OrderServiceImpl#get orderId={}", orderId);
        return toResponseDto(findById(orderId));
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderResponseDto> getByCustomer(Integer customerId) {
        log.debug("OrderServiceImpl#getByCustomer customerId={}", customerId);
        return orderRepository.findAllByCustomer_Id(customerId).stream()
                .map(this::toResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public OrderResponseDto create(CreateOrderRequestDto orderDto) {
        log.debug("OrderServiceImpl#create orderDto={}", orderDto);
        if (orderDto == null) {
            throw new BadRequestException("Request body is null");
        }
        final var createdOrderDto = toResponseDto(orderRepository.save(toOrder(orderDto.validate())));
        kafkaProducer.sendMessage(createdOrderDto.getId(), OrderState.CREATED, createdOrderDto);
        return createdOrderDto;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public OrderResponseDto updateOrderProducts(Integer orderId, UpdateOrderRequestDto orderDto) {
        log.debug("OrderServiceImpl#updateOrderProducts orderId={} orderDto={}", orderId, orderDto);
        final var foundOrder = findById(orderId);
        if (!OrderStatus.NEW.equals(foundOrder.getOrderStatus())) {
            throw new BadRequestException("Can't change products. Order was issued or canceled.");
        }
        if (orderDto == null) {
            throw new BadRequestException("Request body is null");
        }
        foundOrder.setProducts(productService.findAllByIdList(orderDto.validate().getProductsIdList()));
        final var updatedOrder = toResponseDto(foundOrder);
        kafkaProducer.sendMessage(updatedOrder.getId(), OrderState.UPDATED, updatedOrder);
        return updatedOrder;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public OrderResponseDto updateStatus(Integer orderId, OrderStatus status) {
        log.debug("OrderServiceImpl#updateStatus orderId={} status={}", orderId, status);
        final var foundOrder = findById(orderId);
        if (!OrderStatus.NEW.equals(foundOrder.getOrderStatus())) {
            throw new BadRequestException("Can't change status. Order was issued or canceled.");
        }
        foundOrder.setOrderStatus(status);
        final var updatedOrder = toResponseDto(foundOrder);
        kafkaProducer.sendMessage(updatedOrder.getId(), OrderState.UPDATED, updatedOrder);
        return updatedOrder;
    }

    /**
     * Поиск заказа по идентификатору в БД
     *
     * @param orderId идентификатор заказа
     * @return заказ
     */
    private Order findById(Integer orderId) {
        log.debug("OrderServiceImpl#findById orderId={}", orderId);
        return orderRepository.findById(orderId)
                .orElseThrow(() -> new EntityNotFoundException("Order not found by id=" + orderId));
    }

    /**
     * Маппинг DTO создания заказа в сущность
     *
     * @param orderDto DTO создания заказа
     * @return сущность заказа
     */
    private Order toOrder(CreateOrderRequestDto orderDto) {
        log.debug("OrderServiceImpl#toOrder orderDto={}", orderDto);
        final var customerEntity = customerService.findById(orderDto.getCustomerId());
        return Order.builder()
                .customer(customerEntity)
                .products(productService.findAllByIdList(orderDto.getProductsIdList()))
                .orderStatus(OrderStatus.NEW)
                .orderDate(LocalDate.now())
                .build();
    }

    /**
     * Маппинг сущности заказа в ответное DTO заказа
     *
     * @param order сущность заказа
     * @return ответное DTO заказа
     */
    private OrderResponseDto toResponseDto(Order order) {
        final var customer = order.getCustomer();
        return OrderResponseDto.builder()
                .id(order.getId())
                .customer(IdNameDto.builder().id(customer.getId()).name(customer.getFullName()).build())
                .orderDate(order.getOrderDate())
                .orderStatus(order.getOrderStatus())
                .products(order.getProducts().stream()
                        .map(product -> IdNameDto.builder().id(product.getId()).name(product.getName()).build())
                        .collect(Collectors.toList()))
                .build();
    }
}
