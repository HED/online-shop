package ru.troshin.onlineshop.services.impl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.troshin.onlineshop.rest.exception.EntityNotFoundException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Map<String, DefaultUser> users;

    @Autowired
    public UserDetailsServiceImpl(PasswordEncoder passwordEncoder) {
        users = Map.of(
                "Seller", DefaultUser.builder().username("Seller").role(UserRole.SELLER).password(passwordEncoder.encode("123")).build(),
                "Manager", DefaultUser.builder().username("Manager").role(UserRole.MANAGER).password(passwordEncoder.encode("321")).build()
        );
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return Optional.ofNullable(users.get(username))
                .orElseThrow(() -> new EntityNotFoundException("User not found by username=" + username));
    }

    @Getter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    static class DefaultUser implements UserDetails {
        private String username;
        private String password;
        private UserRole role;

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return List.of(new SimpleGrantedAuthority(role.name()));
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    private enum UserRole {
        SELLER, MANAGER
    }
}
