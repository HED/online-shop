package ru.troshin.onlineshop.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;
import ru.troshin.onlineshop.rest.dto.OrderKafkaMessage;
import ru.troshin.onlineshop.rest.dto.OrderResponseDto;
import ru.troshin.onlineshop.rest.dto.OrderState;
import ru.troshin.onlineshop.services.api.KafkaProducer;

@Slf4j
@Service
public class KafkaProducerImpl implements KafkaProducer {

    private final String orderLogTopic;
    private final KafkaTemplate<Integer, OrderKafkaMessage> kt;


    public KafkaProducerImpl(@Value("${order-events-kafka-topic}") String orderLogTopic,
                             KafkaTemplate<Integer, OrderKafkaMessage> kt) {
        this.orderLogTopic = orderLogTopic;
        this.kt = kt;
    }

    @Override
    public void sendMessage(Integer orderId, OrderState state, OrderResponseDto orderDto) {
        log.debug("KafkaProducerImpl#sendMessage orderId={} state={} orderDto={}", orderId, state, orderDto);
        final var message = OrderKafkaMessage.builder()
                .orderId(orderId)
                .orderState(state)
                .orderResponseDto(orderDto)
                .build();
        kt.send(orderLogTopic, orderId, message)
                .addCallback(new ListenableFutureCallback<>() {
                    @Override
                    public void onSuccess(SendResult<Integer, OrderKafkaMessage> result) {
                        log.info(String.format("Success sending order message:%s state:%s", orderDto, state));
                    }
                    @Override
                    public void onFailure(Throwable ex) {
                        log.error(String.format("Failure sending order message:%s state:%s", orderDto, state), ex);
                    }
                });
    }
}
