package ru.troshin.onlineshop.domain.entity;

/**
 * Статус заказа
 */
public enum OrderStatus {
    /**
     * Новый
     */
    NEW,
    /**
     * Выдан
     */
    ISSUED,
    /**
     * Отменён
     */
    CANCELED
}
