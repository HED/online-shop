package ru.troshin.onlineshop.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.troshin.onlineshop.domain.entity.Customer;

/**
 * Репозиторий для работы с сущностью клиента
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
