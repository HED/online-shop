package ru.troshin.onlineshop.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.troshin.onlineshop.domain.entity.Product;

/**
 * Репозиторий для работы с сущностью товара
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
}
