package ru.troshin.onlineshop.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.troshin.onlineshop.domain.entity.Order;

import java.util.List;

/**
 * Репозиторий для работы с сущностью заказа
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
    /**
     * Поиск заказов по идентификатору клиента
     *
     * @param customerId идентификатор клиента
     * @return список сущностей заказов
     */
    List<Order> findAllByCustomer_Id(Integer customerId);
}
