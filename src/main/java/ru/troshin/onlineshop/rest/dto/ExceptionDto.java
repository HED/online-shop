package ru.troshin.onlineshop.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "DTO ошибки")
public class ExceptionDto {
    @Schema(description = "Сообщение")
    private String message;

    /**
     * Построение объекта с описанием ошибки
     *
     * @param message описание ошибки
     * @return объект ошибки
     */
    public static ExceptionDto build(String message) {
        return new ExceptionDto(message);
    }
}
