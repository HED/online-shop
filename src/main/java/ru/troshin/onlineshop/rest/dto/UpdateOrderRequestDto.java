package ru.troshin.onlineshop.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.troshin.onlineshop.rest.exception.BadRequestException;
import ru.troshin.onlineshop.utils.ValidationUtils;

import java.util.List;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "DTO редактирования заказа")
public class UpdateOrderRequestDto {
    @Schema(description = "Список идентификаторов товаров")
    private List<Integer> productsIdList;

    /**
     * Валидация объекта
     *
     * @return this
     */
    public UpdateOrderRequestDto validate() {
        final var productsIdsValidateResults = ValidationUtils.idListValidate("Products", productsIdList);

        if (!productsIdsValidateResults.isEmpty()) {
            throw new BadRequestException(productsIdsValidateResults);
        }
        return this;
    }
}
