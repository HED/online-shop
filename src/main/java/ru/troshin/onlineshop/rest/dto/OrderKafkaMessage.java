package ru.troshin.onlineshop.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Сообщение о действии с заказом
 */
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OrderKafkaMessage {
    /**
     * Идентификатор заказа
     */
    private Integer orderId;
    /**
     * Состояние заказа
     */
    private OrderState orderState;
    /**
     * DTO заказа
     */
    private OrderResponseDto orderResponseDto;
}
