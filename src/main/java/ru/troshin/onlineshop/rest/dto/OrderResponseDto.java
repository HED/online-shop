package ru.troshin.onlineshop.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.troshin.onlineshop.domain.entity.OrderStatus;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "DTO для выдачи данных заказа")
public class OrderResponseDto {
    @Schema(description = "Идентификатор")
    private Integer id;
    @Schema(description = "Клиент")
    private IdNameDto customer;
    @Schema(description = "Дата заказа")
    private LocalDate orderDate;
    @Schema(description = "Статус")
    private OrderStatus orderStatus;
    @Schema(description = "Товары в заказе")
    private List<IdNameDto> products;
}
