package ru.troshin.onlineshop.rest.dto;

/**
 * Состояние заказа
 */
public enum OrderState {
    /**
     * Создан
     */
    CREATED,
    /**
     * Изменён
     */
    UPDATED
}
