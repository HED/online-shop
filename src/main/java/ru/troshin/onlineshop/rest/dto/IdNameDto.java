package ru.troshin.onlineshop.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сокращённое DTO")
public class IdNameDto {
    @Schema(description = "Идентификатор")
    private Integer id;
    @Schema(description = "Название/имя")
    private String name;
}
