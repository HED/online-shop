package ru.troshin.onlineshop.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.troshin.onlineshop.rest.exception.BadRequestException;
import ru.troshin.onlineshop.utils.ValidationUtils;

import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "DTO создания заказа")
public class CreateOrderRequestDto {
    @Schema(description = "Идентификатор клиента")
    private Integer customerId;
    @Schema(description = "Список идентификаторов товаров")
    private List<Integer> productsIdList;

    /**
     * Валидация объекта
     *
     * @return this
     */
    public CreateOrderRequestDto validate() {
        final var validationErrorMessages = new ArrayList<String>();

        ValidationUtils.idValidate("Customer", customerId).ifPresent(validationErrorMessages::add);
        final var productsIdsValidationResults = ValidationUtils.idListValidate("Products", productsIdList);
        if (!productsIdsValidationResults.isEmpty()) {
            validationErrorMessages.addAll(productsIdsValidationResults);
        }

        if (!validationErrorMessages.isEmpty()) {
            throw new BadRequestException(validationErrorMessages);
        }
        return this;
    }
}
