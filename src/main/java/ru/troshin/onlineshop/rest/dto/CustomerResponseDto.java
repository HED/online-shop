package ru.troshin.onlineshop.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "DTO для выдачи данных клиента")
public class CustomerResponseDto {
    @Schema(description = "Идентификатор")
    private Integer id;
    @Schema(description = "ФИО")
    private String fullName;
    @Schema(description = "Номер телефона")
    private String phoneNumber;
}
