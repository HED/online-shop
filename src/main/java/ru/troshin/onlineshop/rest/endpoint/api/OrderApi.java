package ru.troshin.onlineshop.rest.endpoint.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.troshin.onlineshop.domain.entity.OrderStatus;
import ru.troshin.onlineshop.rest.dto.CreateOrderRequestDto;
import ru.troshin.onlineshop.rest.dto.OrderResponseDto;
import ru.troshin.onlineshop.rest.dto.UpdateOrderRequestDto;

import java.util.List;

@Tag(name = "Заказы")
@RequestMapping("/orders")
public interface OrderApi {

    @Operation(summary = "Получение заказа по идентификатору", security = @SecurityRequirement(name = "Basic"))
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority({'SELLER', 'MANAGER'})")
    ResponseEntity<OrderResponseDto> getById(
            @Parameter(description = "Идентификатор заказа", example = "1")
            @PathVariable("id") Integer orderId
    );

    @Operation(summary = "Получение заказов по идентификатору клиента", security = @SecurityRequirement(name = "Basic"))
    @PreAuthorize("hasAnyAuthority({'SELLER', 'MANAGER'})")
    @GetMapping("/client/{id}")
    ResponseEntity<List<OrderResponseDto>> getOrdersByCustomer(
            @Parameter(description = "Идентификатор клиента", example = "1")
            @PathVariable("id") Integer customerId
    );

    @Operation(summary = "Создание нового заказа", security = @SecurityRequirement(name = "Basic"))
    @PreAuthorize("hasAnyAuthority({'SELLER', 'MANAGER'})")
    @PostMapping
    ResponseEntity<OrderResponseDto> createOrder(
            @RequestBody CreateOrderRequestDto orderDto
    );

    @Operation(summary = "Редактирование товаров в заказе", security = @SecurityRequirement(name = "Basic"))
    @PreAuthorize("hasAuthority('MANAGER')")
    @PatchMapping("/{id}")
    ResponseEntity<OrderResponseDto> updateOrderProducts(
            @Parameter(description = "Идентификатор заказа", example = "1")
            @PathVariable("id") Integer orderId,
            @RequestBody UpdateOrderRequestDto orderDto
    );

    @Operation(summary = "Изменение статуса заказа", security = @SecurityRequirement(name = "Basic"))
    @PreAuthorize("hasAuthority('MANAGER')")
    @PatchMapping("/{id}/status")
    ResponseEntity<OrderResponseDto> updateOrderStatus(
            @Parameter(description = "Идентификатор заказа", example = "1")
            @PathVariable("id") Integer orderId,
            @Parameter(description = "Новый статус заказа", example = "ISSUED")
            @RequestParam(name = "status") OrderStatus status
    );
}
