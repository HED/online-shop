package ru.troshin.onlineshop.rest.endpoint.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.troshin.onlineshop.domain.entity.OrderStatus;
import ru.troshin.onlineshop.rest.dto.CreateOrderRequestDto;
import ru.troshin.onlineshop.rest.dto.OrderResponseDto;
import ru.troshin.onlineshop.rest.dto.UpdateOrderRequestDto;
import ru.troshin.onlineshop.rest.endpoint.api.OrderApi;
import ru.troshin.onlineshop.services.api.OrderService;

import java.util.List;

@Slf4j
@RestController
public class OrderController implements OrderApi {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public ResponseEntity<OrderResponseDto> getById(Integer orderId) {
        log.info("Receive request: GET /orders/{}", orderId);
        return ResponseEntity.ok(orderService.get(orderId));
    }

    @Override
    public ResponseEntity<List<OrderResponseDto>> getOrdersByCustomer(Integer customerId) {
        log.info("Receive request: GET /orders/client/{}", customerId);
        return ResponseEntity.ok(orderService.getByCustomer(customerId));
    }

    @Override
    public ResponseEntity<OrderResponseDto> createOrder(CreateOrderRequestDto orderDto) {
        log.info("Receive request: POST /orders body:{}", orderDto);
        var createdOrder = orderService.create(orderDto);
        var location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdOrder.getId())
                .toUri();
        return ResponseEntity.created(location).body(createdOrder);
    }

    @Override
    public ResponseEntity<OrderResponseDto> updateOrderProducts(Integer orderId, UpdateOrderRequestDto orderDto) {
        log.info("Receive request: PATCH /orders/{} body:{}", orderId, orderDto);
        return ResponseEntity.ok(orderService.updateOrderProducts(orderId, orderDto));
    }

    @Override
    public ResponseEntity<OrderResponseDto> updateOrderStatus(Integer orderId, OrderStatus status) {
        log.info("Receive request: PATCH /orders/{}/status?status={}", orderId, status);
        return ResponseEntity.ok(orderService.updateStatus(orderId, status));
    }
}
