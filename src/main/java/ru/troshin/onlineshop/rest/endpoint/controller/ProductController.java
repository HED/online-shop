package ru.troshin.onlineshop.rest.endpoint.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.troshin.onlineshop.rest.dto.IdNameDto;
import ru.troshin.onlineshop.rest.endpoint.api.ProductApi;
import ru.troshin.onlineshop.services.api.ProductService;

import java.util.List;

@Slf4j
@RestController
public class ProductController implements ProductApi {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public ResponseEntity<List<IdNameDto>> getProducts() {
        log.info("Receive request: GET /products");
        return ResponseEntity.ok(productService.getAll());
    }

    @Override
    public ResponseEntity<IdNameDto> getById(Integer productId) {
        log.info("Receive request: GET /products/{}", productId);
        return ResponseEntity.ok(productService.getById(productId));
    }
}
