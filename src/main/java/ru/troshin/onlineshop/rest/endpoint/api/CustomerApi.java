package ru.troshin.onlineshop.rest.endpoint.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.troshin.onlineshop.rest.dto.CustomerResponseDto;

import java.util.List;

@Tag(name = "Клиенты")
@RequestMapping("/customers")
public interface CustomerApi {

    @Operation(summary = "Получение всех клиентов", security = @SecurityRequirement(name = "Basic"))
    @GetMapping
    @PreAuthorize("hasAnyAuthority({'SELLER', 'MANAGER'})")
    ResponseEntity<List<CustomerResponseDto>> getCustomers();

    @Operation(summary = "Получение клиента по идентификатору", security = @SecurityRequirement(name = "Basic"))
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority({'SELLER', 'MANAGER'})")
    ResponseEntity<CustomerResponseDto> getById(
            @Parameter(description = "Идентификатор клиента", example = "1")
            @PathVariable("id") Integer customerId
    );
}
