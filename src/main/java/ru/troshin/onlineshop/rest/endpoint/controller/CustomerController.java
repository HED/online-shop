package ru.troshin.onlineshop.rest.endpoint.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.troshin.onlineshop.rest.dto.CustomerResponseDto;
import ru.troshin.onlineshop.rest.endpoint.api.CustomerApi;
import ru.troshin.onlineshop.services.api.CustomerService;

import java.util.List;

@Slf4j
@RestController
public class CustomerController implements CustomerApi {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public ResponseEntity<List<CustomerResponseDto>> getCustomers() {
        log.info("Receive request: GET /customers");
        return ResponseEntity.ok(customerService.getAll());
    }

    @Override
    public ResponseEntity<CustomerResponseDto> getById(Integer customerId) {
        log.info("Receive request: GET /customers/{}", customerId);
        return ResponseEntity.ok(customerService.getById(customerId));
    }
}
