package ru.troshin.onlineshop.rest.endpoint.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.troshin.onlineshop.rest.dto.ExceptionDto;
import ru.troshin.onlineshop.rest.exception.BadRequestException;
import ru.troshin.onlineshop.rest.exception.EntityNotFoundException;

@Slf4j
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionDto> handleEntityNotFoundException(EntityNotFoundException e) {
        log.error("Handled EntityNotFoundException", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ExceptionDto.build(e.getMessage()));
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionDto> handleEntityNotFoundException(BadRequestException e) {
        log.error("Handled BadRequestException", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ExceptionDto.build(e.getMessage()));
    }
}
