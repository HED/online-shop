package ru.troshin.onlineshop.rest.endpoint.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.troshin.onlineshop.rest.dto.IdNameDto;

import java.util.List;

@Tag(name = "Товары")
@RequestMapping("/products")
public interface ProductApi {

    @Operation(summary = "Получение всех товаров", security = @SecurityRequirement(name = "Basic"))
    @GetMapping
    @PreAuthorize("hasAnyAuthority({'SELLER', 'MANAGER'})")
    ResponseEntity<List<IdNameDto>> getProducts();

    @Operation(summary = "Получение товара по идентификатору", security = @SecurityRequirement(name = "Basic"))
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority({'SELLER', 'MANAGER'})")
    ResponseEntity<IdNameDto> getById(
            @Parameter(description = "Идентификатор товара", example = "1")
            @PathVariable("id") Integer productId
    );
}
