package ru.troshin.onlineshop.rest.exception;

import java.util.List;

public class BadRequestException extends RuntimeException {
    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(List<String> validationErrorMessages) {
        super(validationErrorMessages.stream().reduce((m1, m2) -> m1 + "\n" + m2).orElse(""));
    }
}
