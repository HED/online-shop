package ru.troshin.onlineshop.services.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.troshin.onlineshop.domain.entity.Customer;
import ru.troshin.onlineshop.domain.entity.Order;
import ru.troshin.onlineshop.domain.entity.OrderStatus;
import ru.troshin.onlineshop.domain.entity.Product;
import ru.troshin.onlineshop.domain.repository.CustomerRepository;
import ru.troshin.onlineshop.domain.repository.OrderRepository;
import ru.troshin.onlineshop.domain.repository.ProductRepository;
import ru.troshin.onlineshop.rest.dto.CreateOrderRequestDto;
import ru.troshin.onlineshop.rest.dto.IdNameDto;
import ru.troshin.onlineshop.rest.dto.OrderResponseDto;
import ru.troshin.onlineshop.rest.dto.OrderState;
import ru.troshin.onlineshop.rest.dto.UpdateOrderRequestDto;
import ru.troshin.onlineshop.rest.exception.BadRequestException;
import ru.troshin.onlineshop.services.api.KafkaProducer;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class OrderServiceImplTest {

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    @MockBean
    private KafkaProducer kafkaProducer;

    @Captor
    private ArgumentCaptor<Integer> intArgCaptor;
    @Captor
    private ArgumentCaptor<OrderState> orderStateArgCaptor;
    @Captor
    private ArgumentCaptor<OrderResponseDto> orderRespDtoArgCaptor;


    private Product product1;
    private Product product2;
    private Customer customer1;
    private Customer customer2;

    @BeforeEach
    void setUp() {
        product1 = productRepository.save(Product.builder().name("Пенал").build());
        product2 = productRepository.save(Product.builder().name("Рюкзак").build());
        customer1 = customerRepository.save(Customer.builder()
                .fullName("Петров Пётр Петрович")
                .phoneNumber("9998887766")
                .build());
        customer2 = customerRepository.save(Customer.builder()
                .fullName("Иванов Иван Иванович")
                .phoneNumber("9997778866")
                .build());
        orderRepository.deleteAll();
    }

    @Test
    @DisplayName("Получение заказа по идентификатору")
    void get() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.NEW)
                        .build());

        final var foundOrder = orderService.get(savedOrder.getId());

        assertEquals(savedOrder.getId(), foundOrder.getId());
        assertEquals(savedOrder.getOrderStatus(), foundOrder.getOrderStatus());
        assertEquals(savedOrder.getOrderDate(), foundOrder.getOrderDate());
        assertEquals(1, foundOrder.getProducts().size());
        assertEquals(savedOrder.getProducts().get(0).getId(), foundOrder.getProducts().get(0).getId());
        assertEquals(savedOrder.getProducts().get(0).getName(), foundOrder.getProducts().get(0).getName());
        assertEquals(savedOrder.getCustomer().getId(), foundOrder.getCustomer().getId());
        assertEquals(savedOrder.getCustomer().getFullName(), foundOrder.getCustomer().getName());
    }

    @Test
    @DisplayName("Получение заказа по идентификатору клиента")
    void getByCustomer() {
        final var savedOrder1 = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.NEW)
                        .build());
        orderRepository.save(
                Order.builder()
                        .customer(customer2)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.NEW)
                        .build());

        final var foundOrder = orderService.getByCustomer(customer1.getId());

        assertEquals(1, foundOrder.size());
        assertEquals(savedOrder1.getId(), foundOrder.get(0).getId());
    }

    @Test
    @DisplayName("Сохранение заказа")
    void create() {
        final var saveOrderRequest = CreateOrderRequestDto.builder()
                .customerId(customer1.getId())
                .productsIdList(List.of(product1.getId(), product2.getId()))
                .build();

        doNothing().when(kafkaProducer).sendMessage(
                intArgCaptor.capture(),
                orderStateArgCaptor.capture(),
                orderRespDtoArgCaptor.capture()
        );

        final var savedOrder = orderService.create(saveOrderRequest);

        assertEquals(saveOrderRequest.getCustomerId(), savedOrder.getCustomer().getId());
        assertEquals(
                saveOrderRequest.getProductsIdList(),
                savedOrder.getProducts().stream().map(IdNameDto::getId).collect(Collectors.toList())
        );
        verify(kafkaProducer, times(1))
                .sendMessage(intArgCaptor.capture(), orderStateArgCaptor.capture(), orderRespDtoArgCaptor.capture());
        assertEquals(intArgCaptor.getValue(), savedOrder.getId());
        assertEquals(orderStateArgCaptor.getValue(), OrderState.CREATED);
        assertNotNull(orderRespDtoArgCaptor.getValue());
    }

    @Test
    @DisplayName("Сохранение заказа без тела")
    void createWithoutRequestBody() {
        assertThrows(BadRequestException.class, () -> orderService.create(null));
    }

    @Test
    @DisplayName("Сохранение заказа без идентификатора клиента")
    void createWithoutCustomerId() {
        assertThrows(BadRequestException.class, () -> orderService.create(CreateOrderRequestDto.builder().build()));
    }

    @Test
    @DisplayName("Сохранение заказа с отрицательным идентификатором клиента")
    void createWithInvalidCustomerId() {
        assertThrows(
                BadRequestException.class,
                () -> orderService.create(CreateOrderRequestDto.builder().customerId(-1).build())
        );
    }

    @Test
    @DisplayName("Сохранение без идентификатора продуктов")
    void createWithoutProducts() {
        assertThrows(
                BadRequestException.class,
                () -> orderService.create(CreateOrderRequestDto.builder().customerId(customer1.getId()).build())
        );
    }

    @Test
    @DisplayName("Сохранение с отрицательным идентификатором продукта")
    void createWithInvalidProducts() {
        assertThrows(
                BadRequestException.class,
                () -> orderService.create(CreateOrderRequestDto.builder()
                        .customerId(customer1.getId())
                        .productsIdList(List.of(-1))
                        .build())
        );
    }

    @Test
    @DisplayName("Изменение списка продуктов в заказе")
    void updateOrderProducts() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.NEW)
                        .build());

        doNothing().when(kafkaProducer).sendMessage(
                intArgCaptor.capture(),
                orderStateArgCaptor.capture(),
                orderRespDtoArgCaptor.capture()
        );

        final var updatedOrder = orderService.updateOrderProducts(
                savedOrder.getId(),
                UpdateOrderRequestDto.builder().productsIdList(List.of(product2.getId())).build()
        );

        assertEquals(1, updatedOrder.getProducts().size());
        assertEquals(product2.getId(), updatedOrder.getProducts().get(0).getId());
        verify(kafkaProducer, times(1))
                .sendMessage(intArgCaptor.capture(), orderStateArgCaptor.capture(), orderRespDtoArgCaptor.capture());
        assertEquals(intArgCaptor.getValue(), updatedOrder.getId());
        assertEquals(orderStateArgCaptor.getValue(), OrderState.UPDATED);
        assertNotNull(orderRespDtoArgCaptor.getValue());
    }

    @Test
    @DisplayName("Изменение списка продуктов в заказе со статусом 'Выдан'")
    void updateOrderProductsOfOrderWithIssuedStatus() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.ISSUED)
                        .build());

        assertThrows(BadRequestException.class, () -> orderService.updateOrderProducts(
                savedOrder.getId(),
                UpdateOrderRequestDto.builder().productsIdList(List.of(product2.getId())).build()
        ));
    }

    @Test
    @DisplayName("Изменение списка продуктов в заказе со статусом 'Отменён'")
    void updateOrderProductsOfOrderWithCanceledStatus() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.CANCELED)
                        .build());

        assertThrows(BadRequestException.class, () -> orderService.updateOrderProducts(
                savedOrder.getId(),
                UpdateOrderRequestDto.builder().productsIdList(List.of(product2.getId())).build()
        ));
    }

    @Test
    @DisplayName("Изменение списка продуктов без продуктов")
    void updateOrderProductsWithoutProductId() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.NEW)
                        .build());

        assertThrows(BadRequestException.class, () -> orderService.updateOrderProducts(
                savedOrder.getId(), UpdateOrderRequestDto.builder().build()
        ));
    }

    @Test
    @DisplayName("Изменение списка продуктов без тела запроса")
    void updateOrderProductsWithoutRequestBody() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.NEW)
                        .build());

        assertThrows(BadRequestException.class, () -> orderService.updateOrderProducts(
                savedOrder.getId(), null
        ));
    }


    @Test
    @DisplayName("Изменение списка продуктов с некорректным идентификатором")
    void updateOrderProductsWithInvalidProductId() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.NEW)
                        .build());

        assertThrows(BadRequestException.class, () -> orderService.updateOrderProducts(
                savedOrder.getId(),
                UpdateOrderRequestDto.builder().productsIdList(List.of(-2)).build()
        ));
    }

    @Test
    @DisplayName("Обновление статуса")
    void updateStatus() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.NEW)
                        .build());

        doNothing().when(kafkaProducer).sendMessage(
                intArgCaptor.capture(),
                orderStateArgCaptor.capture(),
                orderRespDtoArgCaptor.capture()
        );

        final var updatedOrder = orderService.updateStatus(savedOrder.getId(), OrderStatus.ISSUED);

        assertEquals(savedOrder.getId(), updatedOrder.getId());
        assertEquals(OrderStatus.ISSUED, updatedOrder.getOrderStatus());
        verify(kafkaProducer, times(1))
                .sendMessage(intArgCaptor.capture(), orderStateArgCaptor.capture(), orderRespDtoArgCaptor.capture());
        assertEquals(intArgCaptor.getValue(), savedOrder.getId());
        assertEquals(orderStateArgCaptor.getValue(), OrderState.UPDATED);
        assertNotNull(orderRespDtoArgCaptor.getValue());
    }

    @Test
    @DisplayName("Обновление статуса заказа со статусом 'Выдан'")
    void updateStatusOfOrderWithIssuedStatus() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.ISSUED)
                        .build());

        assertThrows(BadRequestException.class, () -> orderService.updateStatus(savedOrder.getId(), OrderStatus.NEW));
    }

    @Test
    @DisplayName("Обновление статуса заказа со статусом 'Отменён'")
    void updateStatusOfOrderWithCanceledStatus() {
        final var savedOrder = orderRepository.save(
                Order.builder()
                        .customer(customer1)
                        .products(List.of(product1))
                        .orderDate(LocalDate.now())
                        .orderStatus(OrderStatus.CANCELED)
                        .build());

        assertThrows(BadRequestException.class, () -> orderService.updateStatus(savedOrder.getId(), OrderStatus.NEW));
    }
}